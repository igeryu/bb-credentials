package com.magipixel.bountyboard.auth.credentials.model;

import org.springframework.data.repository.CrudRepository;

public interface UserRepository extends CrudRepository<User, Long> {
    User findByUsername(final String username);
}