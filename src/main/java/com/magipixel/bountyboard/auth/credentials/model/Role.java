package com.magipixel.bountyboard.auth.credentials.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum Role {
    PARENT();

    private final String value;

    Role() {
        this.value = "PARENT";
    }

    @JsonValue
    @Override
    public String toString() {
        return "ROLE_" + this.value;
    }
}