package com.magipixel.bountyboard.auth.credentials.config;

import com.magipixel.bountyboard.auth.credentials.model.User;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@RequiredArgsConstructor
public class PasswordEncryptionListener {
    private final PasswordEncoder encoder;

    @PrePersist
    @PreUpdate
    public void encryptPassword(final User user) {
        user.setPassword(encoder.encode(user.getPassword()));
    }
}