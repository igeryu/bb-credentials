package com.magipixel.bountyboard.auth.credentials.model;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.validation.ConstraintViolationException;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Tag("unit")
class UserRepositoryTest {

    @Autowired
    private UserRepository repository;

    @AfterEach
    void cleanUpUsers() {
        repository.deleteAll();
    }

    @Test
    @DisplayName("Username field is unique")
    void username_is_unique() {
        // Arrange
        final String duplicateUsername = "username_a";
        //    Add User with username "username_a"
        final User userA = new User();
        userA.setUsername(duplicateUsername);
        userA.setRole(Role.PARENT);
        userA.setPassword("can_be_anything");
        repository.save(userA);

        //    Add another User with username "username_a"
        final User userB = new User();
        userB.setUsername(duplicateUsername);
        userB.setRole(Role.PARENT);
        userB.setPassword("can_be_anything");

        // Act / Assert
        assertThrows(DataIntegrityViolationException.class, () -> repository.save(userB));
    }

    @Test
    @DisplayName("Role field is required")
    void role_is_required() {
        // Arrange
        //    Add User with null Role
        final User user = new User();
        user.setUsername("can_be_anything_a");
        user.setRole(null);
        user.setPassword("can_be_anything");

        // Act / Assert
        assertThrows(ConstraintViolationException.class, () -> repository.save(user));
    }

    @Test
    void findByUsername() {
        // Arrange
        //    Add first distraction User
        final User userA = new User();
        userA.setUsername("username_a");
        userA.setRole(Role.PARENT);
        userA.setPassword("can_be_anything");
        repository.save(userA);

        //    Add target User entity to search for
        final User userB = new User();
        final String usernameB = "username_b";
        userB.setUsername(usernameB);
        userB.setRole(Role.PARENT);
        userB.setPassword("can_be_anything");
        final Long idB = repository.save(userB).getId();

        //    Add second distraction User
        final User userC = new User();
        userC.setUsername("username_c");
        userC.setRole(Role.PARENT);
        userC.setPassword("can_be_anything");
        repository.save(userC);

        // Act
        final User result = repository.findByUsername(usernameB);

        // Assert
        assertThat("Repository returned null", result, notNullValue());
        userB.setId(idB);
        assertThat("Repository returned wrong User", result, is(equalTo(userB)));

    }

}