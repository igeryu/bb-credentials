package com.magipixel.bountyboard.auth.credentials;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.hamcrest.core.IsNot.not;
import static org.hamcrest.core.IsNull.notNullValue;
import static org.hamcrest.core.IsNull.nullValue;
import static org.hamcrest.text.IsEmptyString.isEmptyString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@AutoConfigureMockMvc
@SpringBootTest
@Tag("integration")
class StatusCodesIT {

    @MockBean
    private PasswordEncoder encoder;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    @DisplayName("Should return 409 Conflict, when duplicate account created")
    void shouldReturn_409Conflict_whenDuplicateAccountCreated() throws Exception {
        // Arrange
        final String duplicatedEmail = "xyz@abc.com";
        final String user = "{\"username\": \"" + duplicatedEmail + "\", \"role\": \"ROLE_PARENT\"}";
        mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(user)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status()
                        .isCreated());

        // Act / Assert
        final String resultAsString = mockMvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(user)
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status()
                        .isConflict())
                .andReturn()
                .getResponse()
                .getContentAsString();

        assertThat("Expected response object", resultAsString, not(isEmptyString()));
        final Map<String, Object> resultAsMap =
                mapper.readValue(resultAsString, new TypeReference<Map<String, Object>>() {
                });
        assertThat("Expected 'cause' field to not exist", resultAsMap.get("cause"), nullValue());
        assertThat("Expected 'message' field to exist", resultAsMap.get("message"), notNullValue());
        final String expectedMessage = "Account already exists";
        assertThat("Expected 'message' field to have correct message",
                resultAsMap.get("message"), equalTo(expectedMessage));

    }
}
