package com.magipixel.bountyboard.auth.credentials.config;

import com.magipixel.bountyboard.auth.credentials.model.Role;
import com.magipixel.bountyboard.auth.credentials.model.User;
import com.magipixel.bountyboard.auth.credentials.model.UserRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;

@SpringBootTest
@ExtendWith(SpringExtension.class)
@Tag("integration")
class PasswordEncryptionListenerIT {

    @SpyBean
    private PasswordEncoder encoder;

    @Autowired
    private UserRepository repository;

    @Test
    @DisplayName("Listener should be invoked upon creation of new User")
    void listenerShouldBeInvokedUponCreationOfNewUser() {
        final String expectedOriginalPassword = "abc1234original";
        final User originalUser = new User();
        originalUser.setPassword(expectedOriginalPassword);
        originalUser.setRole(Role.PARENT);

        repository.save(originalUser);

        then(encoder).should().encode(expectedOriginalPassword);
    }

    @Test
    @DisplayName("Listener should be invoked upon update of existing User")
    void listenerShouldBeInvokedUponUpdateOfExistingUser() {
        final User originalUser = new User();
        originalUser.setPassword("original_password");
        originalUser.setRole(Role.PARENT);
        given(encoder.encode(anyString())).willReturn("encrypted_password");
        final long userId = repository.save(originalUser).getId();

        final User updatedUser = new User();
        updatedUser.setId(userId);
        updatedUser.setRole(Role.PARENT);
        final String expectedPassword = "some_new_password";
        updatedUser.setPassword(expectedPassword);
        repository.save(updatedUser);

        then(encoder).should().encode(expectedPassword);
    }

    @Test
    @DisplayName("Username should be unique")
    void usernameShouldBeUnique() {
        // Arrange
        final String username = "abc1234original";
        final User originalUser = new User();
        originalUser.setUsername(username);
        final String expectedPassword = "expected password";
        originalUser.setPassword(expectedPassword);
        originalUser.setRole(Role.PARENT);
        given(encoder.encode(expectedPassword)).willReturn(expectedPassword);
        repository.save(originalUser);

        final User duplicateUser = new User();
        duplicateUser.setUsername(username);
        final String duplicatePassword = "can_be_anything";
        duplicateUser.setPassword(duplicatePassword);
        duplicateUser.setRole(Role.PARENT);
        given(encoder.encode(duplicatePassword)).willReturn(duplicatePassword);

        // Act / Assert
        assertThrows(DataIntegrityViolationException.class,
                () -> repository.save(duplicateUser));
    }

}