package com.magipixel.bountyboard.auth.credentials.model;

import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;

@Tag("unit")
class RoleTest {

    @Test
    void verifyEnumOrder() {
        assertThat("PARENT should have ordinal value of 0", Role.PARENT.ordinal(), equalTo(0));
    }

}