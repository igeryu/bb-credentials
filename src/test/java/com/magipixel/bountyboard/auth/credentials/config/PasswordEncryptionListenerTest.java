package com.magipixel.bountyboard.auth.credentials.config;

import com.magipixel.bountyboard.auth.credentials.model.User;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.mockito.BDDMockito.given;

@ExtendWith(MockitoExtension.class)
@Tag("unit")
class PasswordEncryptionListenerTest {
    @InjectMocks
    private PasswordEncryptionListener listener;

    @Mock
    private PasswordEncoder encoder;

    @Test
    void test() {
        final String expectedPassword = "abc123xyz";
        final String originalPassword = "original_password";
        given(encoder.encode(originalPassword)).willReturn(expectedPassword);
        final User user = new User();
        user.setPassword(originalPassword);

        listener.encryptPassword(user);

        assertThat(user.getPassword(), equalTo(expectedPassword));
    }
}